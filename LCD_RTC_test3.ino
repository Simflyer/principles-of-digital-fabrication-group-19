// Importing libraries
// Wire allows communication with I2C devices
#include <Wire.h>
// LiquidCrystal_I2C controls LCD
// Recommended Library version for LiquidCrystal_I2C:1.1
// I used version 1.1.2, differences shouldn't be huge... rigth?
#include <LiquidCrystal_I2C.h>

// Library for Real-Time Clock module
#include "RTClib.h"

RTC_DS3231 rtc;

char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

int P1 = 6; // Set alarm
int P2 = 7; // Set timer
int P3 = 8; // Change time
int P4 = 9; // nothing?

#define LLED 12
#define RLED 13

#define LSERVO 3
#define RSERVO 5

int currentHour;
int currentMinute;

int alarmHour;
int alarmMinute;
int alarmMenu; // 0=DateAndClock, 1=setAlarmHour, 2=setAlarmMinute
boolean alarmActive = false;

int timerHour;
int timerMinute;
int timerMenu; // 0=DateAndClock, 1=setTimerHour, 2=setTimerMinute

// Set the LCD address to 0x27 for a 16 columns and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);

// LiquidCrystal_I2C library has two print functions:
// lcd.print() prints one character at the cursor location

// lcd.println() prints one line of text but In my use it had a huge flaw: 
// Apparently the function tries to print the whole line into the place of one character,
// which obviously didn't work

// So I made alternative function myself to print one line of text into the LCD
void print_lcd_ln(String lcd_ln, int column, int row) {
  for (int i=column; i < lcd_ln.length(); i = i + 1) {
    lcd.setCursor(i,row);
    lcd.print(lcd_ln.substring(i,i + 1));
  }
}

// When using lcd.println, there will be vertical lines at the end
// Using this function for formatting will replace them with empty spaces
String make_str(String str){
    for(int i = 0; i < (16 - str.length()); i++) {
        str += ' ';
    }
    return str;
}

void setup()
{
  Serial.begin(57600);
  Wire.begin();
  
  // Initialize the LCD
  lcd.init();

  // Turn on the LCD backlight
  lcd.backlight();
  lcd.clear();

  pinMode(P1, INPUT_PULLUP);
  pinMode(P2, INPUT_PULLUP);
  pinMode(P3, INPUT_PULLUP);
  pinMode(P4, INPUT_PULLUP);
  pinMode(LLED, OUTPUT);
  pinMode(RLED, OUTPUT);
  
  // String for testing my function (Must be String type!)
  /*String test_msg = "Hello World!";
  print_lcd_ln(test_msg);
  lcd.setCursor(0,1);
  lcd.println("Line2           ");*/

  
#ifndef ESP8266
  while (!Serial); // wait for serial port to connect. Needed for native USB
#endif

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    Serial.flush();
    while (1) delay(10);
  }

  if (rtc.lostPower()) {
    Serial.println("RTC lost power, let's set the time!");
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }

  // When time needs to be re-set on a previously configured device, the
  // following line sets the RTC to the date & time this sketch was compiled
  // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  // This line sets the RTC with an explicit date & time, for example to set
  // January 21, 2014 at 3am you would call:
  // rtc.adjust(DateTime(2022, 04, 29, 17, 19, 0));
  
  // LINE ABOVE SHOULD BE RAN ONLY WHEN SETTING TIME  
  
}

void loop()
{
  DateTime now = rtc.now();
  
  if (digitalRead(P1) == LOW) {
    lcd.clear();
    alarmMenu = alarmMenu + 1;
    delay(200);
  }
  
  if (alarmMenu == 0) {
    displayDateTime();
  }
  if (alarmMenu == 1) {
    alarmHour = setAlarmHour();
  }
  if (alarmMenu == 2) {
    alarmMinute = setAlarmMinute();
  }
  if (alarmMenu > 2) {
    alarmMenu = 0;
  }

  Serial.println("alarmHour");
  Serial.println(alarmHour);
  Serial.println("alarmMinute");
  Serial.println(alarmMinute);
  Serial.println(now.hour(), DEC);
  Serial.print(":");
  Serial.print(now.minute(), DEC);
  
  
  if (now.hour() == alarmHour && now.minute() == alarmMinute && alarmActive == true) {
    for (int i=0; i < 20; i++) {
      digitalWrite(LLED, HIGH);
      digitalWrite(RLED, HIGH);
      delay(200);
      digitalWrite(LLED, LOW);
      digitalWrite(RLED, LOW);
      delay(200);
    }
    alarmActive = false;
  }
  
  // This is not necessary, it's for testing purposes
  /*
  Serial.print(now.year(), DEC);
  Serial.print('/');
  Serial.print(now.month(), DEC);
  Serial.print('/');
  Serial.print(now.day(), DEC);
  Serial.print(' ');
  Serial.print(now.hour(), DEC);
  Serial.print(':');
  Serial.print(now.minute(), DEC);
  Serial.print(':');
  Serial.print(now.second(), DEC);
  Serial.println();
  */
  
}

// Shows current date and time
void displayDateTime() {
  DateTime now = rtc.now();
  currentHour = now.hour();
  currentMinute = now.minute();

  // Creating buffers and using .toString() for easier formatting
  char buffer_date[] = "DD/MM/YYYY";
  print_lcd_ln("Date: ", 0, 0);
  lcd.println(now.toString(buffer_date));

  // mm should stand for minutes and hh for hours
  // But for some reason they are swapped in the library
  char buffer_time[] = "hh:mm:ss";
  print_lcd_ln("Time: ", 0, 1);
  lcd.println(now.toString(buffer_time));
  lcd.setCursor(14, 1);
  lcd.print(" ");
  lcd.setCursor(15, 1);
  lcd.print(" ");
}

// Set hour for alarm
int setAlarmHour() {
  print_lcd_ln("Set alarm hour", 0, 0);
  lcd.setCursor(7,1);
  if (currentHour < 10) {
    lcd.print("0");
    lcd.setCursor(8,1);
    lcd.print(currentHour);
  } else {
    lcd.println(currentHour);
    lcd.setCursor(9,1);
    lcd.print("  ");
  }
  
   if (digitalRead(P3) == LOW) {
    if (currentHour == 0) {
      currentHour = 23;
      delay(200);
    } else {
      currentHour = currentHour - 1;
      delay(200);
    }
  }

  if (digitalRead(P4) == LOW) {
    if (currentHour == 23) {
      currentHour = 0;
      delay(200);
    } else {
      currentHour = currentHour + 1;
      delay(200);
    }
  }

  if (digitalRead(P2) == LOW) {
    lcd.clear();
    print_lcd_ln("Alarm cancelled",0,0);
    alarmActive = false;
    alarmMenu = 0;
    delay(5000);
    return;
  }
  
  if (digitalRead(P1) == LOW) {
    return currentHour;
    delay(200);
  }
}

// Set alarm minute
int setAlarmMinute() {
  DateTime now = rtc.now();
  
  print_lcd_ln("Set alarm minute", 0, 0);
  lcd.setCursor(7,1);
  if (currentMinute < 10) {
    lcd.print("0");
    lcd.setCursor(8,1);
    lcd.print(currentMinute);
  } else {
    lcd.println(currentMinute);
    lcd.setCursor(9,1);
    lcd.print("  ");
  }

   if (digitalRead(P3) == LOW) {
    if (currentMinute == 0) {
      currentMinute = 59;
      delay(200);
    } else {
      currentMinute = currentMinute - 1;
      delay(200);
    }
  }

  if (digitalRead(P4) == LOW) {
    if (currentMinute == 59) {
      currentMinute = 0;
      delay(200);
    } else {
      currentMinute = currentMinute + 1;
      delay(200);
    }
  }

  if (digitalRead(P2) == LOW) {
    lcd.clear();
    print_lcd_ln("Alarm cancelled",0,0);
    alarmActive = false;
    delay(5000);
    alarmMenu = 0;
    return;
  }
  
  if (digitalRead(P1) == LOW && currentMinute != now.minute()) {
    alarmActive = true;
    return currentMinute;
    delay(200);
  }
}
