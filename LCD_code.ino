// Importing libraries
// Wire allows communication with I2C devices
#include <Wire.h>
// LiquidCrystal_I2C controls LCD
// Recommended Library version for LiquidCrystal_I2C:1.1
// I used version 1.1.2, differences shouldn't be huge... rigth?
#include <LiquidCrystal_I2C.h>


// Set the LCD address to 0x27 for a 16 columns and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);

// LiquidCrystal_I2C library has two print functions:
// lcd.print() prints one character at the cursor location

// lcd.println() prints one line of text but In my use it had a huge flaw: 
// Apparently the function tries to print the whole line into the place of one character,
// which obviously didn't work

// So I made alternative function myself to print one line of text into the LCD
void print_lcd_ln(String lcd_ln) {
  for (int i=0; i < lcd_ln.length(); i = i + 1) {
    lcd.setCursor(i,0);
    lcd.print(lcd_ln.substring(i,i + 1));
  }
}

void setup()
{
  Wire.begin();
  
  // Initialize the LCD
  lcd.init();

  // Turn on the LCD backlight
  lcd.backlight();

  
  // String for testing my function (Must be String type!)
  String test_msg = "Hello World!";
  print_lcd_ln(test_msg);
  
}

void loop()
{
  
}
