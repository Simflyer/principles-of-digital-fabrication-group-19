// Importing libraries
// Wire allows communication with I2C devices
#include <Wire.h>
// LiquidCrystal_I2C controls LCD
// Recommended Library version for LiquidCrystal_I2C:1.1
// I used version 1.1.2, differences shouldn't be huge... rigth?
#include <LiquidCrystal_I2C.h>

// Library for Real-Time Clock module
#include <DS3231.h>

RTClib myRTC;

// Set the LCD address to 0x27 for a 16 columns and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);

// LiquidCrystal_I2C library has two print functions:
// lcd.print() prints one character at the cursor location

// lcd.println() prints one line of text but In my use it had a huge flaw: 
// Apparently the function tries to print the whole line into the place of one character,
// which obviously didn't work

// So I made alternative function myself to print one line of text into the LCD
void print_lcd_ln(String lcd_ln) {
  for (int i=0; i < lcd_ln.length(); i = i + 1) {
    lcd.setCursor(i,0);
    lcd.print(lcd_ln.substring(i,i + 1));
  }
}

// When using lcd.println, there will be vertical lines at the end
// Using this function for formatting will remove them
String make_str(char str){
    String new_str = String(str);
    for(int i = 0; i < (16 - new_str.length()); i++) {
        new_str += ' ';  
    }
    return new_str;
}

void setup()
{
  Serial.begin(57600);
  Wire.begin();
  
  // Initialize the LCD
  lcd.init();

  // Turn on the LCD backlight
  lcd.backlight();
  
  // String for testing my function (Must be String type!)
  /*String test_msg = "Hello World!";
  print_lcd_ln(test_msg);
  lcd.setCursor(0,1);
  lcd.println("Line2           ");*/
  
}

void loop()
{
  delay(1000);
  DateTime now = myRTC.now();

  lcd.setCursor(0,0);
  lcd.println(now.year(), DEC);
  lcd.setCursor(4,0);
  lcd.print(":");
  lcd.setCursor(5,0);
  lcd.println(now.month(), DEC);
  lcd.setCursor(7,0);
  lcd.print(":");
  lcd.setCursor(8,0);
  lcd.println(now.day(), DEC);

  lcd.setCursor(1,0);
  //lcd.print(now.hour(), DEC + ":" + now.minute(), DEC);
  
  Serial.print(now.year(), DEC);
  Serial.print('/');
  Serial.print(now.month(), DEC);
  Serial.print('/');
  Serial.print(now.day(), DEC);
  Serial.print(' ');
  Serial.print(now.hour(), DEC);
  Serial.print(':');
  Serial.print(now.minute(), DEC);
  Serial.print(':');
  Serial.print(now.second(), DEC);
  Serial.println();
  
}
