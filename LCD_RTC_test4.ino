// Importing libraries
// Wire allows communication with I2C devices
#include <Wire.h>
// LiquidCrystal_I2C controls LCD
// Recommended Library version for LiquidCrystal_I2C:1.1
// I used version 1.1.2, differences shouldn't be huge... rigth?
#include <LiquidCrystal_I2C.h>

// Library for Real-Time Clock module
#include "RTClib.h"

// Library for controlling servo
#include <Servo.h>

RTC_DS3231 rtc;

char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

// Define pins
int P1 = 6; // Set alarm
int P2 = 7; // Set timer
int P3 = 8; // Change time
int P4 = 9; // Show alarm and timer

#define LLED 12
#define RLED 13

#define LSERVO 3
#define RSERVO 5

const int buzzer = 10; // CHECK THE CORRECT PIN!

// define servos
Servo lservo;
Servo rservo;

// Define variables
int getCurrentYear;
int getCurrentMonth;
int getCurrentDay;
int getCurrentHour;
int getCurrentMinute;

int alarmHour; // Final hour value of the alarm
int alarmMinute; // Final minute value of the alarm
int alarmHourSetter; // Used in setAlarmHour() to set value for alarmHour
int alarmMinuteSetter; // Used in setAlarmMinute() to set value for alarmMinute
int alarmMenu; // 0=showDateAndClock, 1=setAlarmHour, 2=setAlarmMinute
boolean alarmActive = false;

int timerHour; // Final hour value of the timer
int timerMinute; // Final minute value of the timer
int timerHourSetter; // Used in setTimerHour() to set value for timerHour
int timerMinuteSetter; // Used in setTimerMinute() to set value for timerMinute
int timerMenu; // 0=showDateAndClock, 1=setTimerHour, 2=setTimerMinute
boolean timerActive = false;

boolean dateAndTimeChanged = false;

// Set the LCD address to 0x27 for a 16 columns and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);

// LiquidCrystal_I2C library has two print functions:
// lcd.print() prints one character at the cursor location

// lcd.println() prints one line of text but In my use it had a huge flaw: 
// Apparently the function tries to print the whole line into the place of one character,
// which obviously didn't work

// EDIT: After fixing the connection issues with the LCD and I2C module, the flaw isn't that huge anymore:
// Now it only prints some extra vertical lines after the line, which can be replaced with extra spaces
// I'm still keeping the my own function, since it's handy when writing static text
void print_lcd_ln(String lcd_ln, int column, int row) {
  for (int i=column; i < lcd_ln.length(); i = i + 1) {
    lcd.setCursor(i,row);
    lcd.print(lcd_ln.substring(i,i + 1));
  }
}

void setup()
{
  // Begin Serial
  Serial.begin(57600);

  // Begin I2C modules
  Wire.begin();
  
  // Initialize the LCD
  lcd.init();

  // Turn on the LCD backlight
  lcd.backlight();

  // Clear the LCD
  lcd.clear();

  // Set pinmodes
  pinMode(P1, INPUT_PULLUP);
  pinMode(P2, INPUT_PULLUP);
  pinMode(P3, INPUT_PULLUP);
  pinMode(P4, INPUT_PULLUP);
  pinMode(LLED, OUTPUT);
  pinMode(RLED, OUTPUT);
  pinMode(buzzer, OUTPUT);

  // Attach servos to the pins
  lservo.attach(LSERVO);
  rservo.attach(RSERVO);
  
#ifndef ESP8266
  while (!Serial); // wait for serial port to connect. Needed for native USB
#endif

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    Serial.flush();
    while (1) delay(10);
  }

  if (rtc.lostPower()) {
    Serial.println("RTC lost power, let's set the time!");
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }

  // When time needs to be re-set on a previously configured device, the
  // following line sets the RTC to the date & time this sketch was compiled
  // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  // This line sets the RTC with an explicit date & time, for example to set
  // April 29, 2022 at 5:19PM you would call:
  
  // rtc.adjust(DateTime(2022, 04, 29, 17, 19, 0));
  
  // LINE ABOVE SHOULD ONLY BE RAN WHEN SETTING TIME  
}

void loop()
{
  DateTime now = rtc.now();

  // Move forward in alarm menu
  if (digitalRead(P1) == LOW) {
    lcd.clear();
    alarmMenu = alarmMenu + 1;
    delay(200);
  }
  if (alarmMenu == 0) {
    displayDateTime();
  }
  if (alarmMenu == 1) {
    alarmHour = setAlarmHour();
  }
  if (alarmMenu == 2) {
    alarmMinute = setAlarmMinute();
  }
  if (alarmMenu > 2) {
    alarmMenu = 0;
  }

  // Move forward in timer menu
  if (digitalRead(P2) == LOW) {
    lcd.clear();
    timerMenu = timerMenu + 1;
    delay(200);
  }
  if (timerMenu == 0) {
    displayDateTime();
  }
  if (timerMenu == 1) {
    timerHour = setTimerHour();
  }
  if (timerMenu == 2) {
    timerMinute = setTimerMinute();
  }
  if (timerMenu > 2) {
    timerMenu = 0;
  }

  // Activate active alarm at the correct time and then deactivate it
  if (now.hour() == alarmHour && now.minute() == alarmMinute && alarmActive == true) {
    lcd.clear();
    Activate("alarm");
  }

  // Activate active timer at the correct time and then deactivate it
  if (now.hour() == timerHour && now.minute() == timerMinute && timerActive == true) {
    lcd.clear();
    Activate("timer");
  }

  if (digitalRead(P3) == LOW) {
    lcd.clear();
    int ye = changeYear();
    int mo = changeMonth();
    int da = changeDay();
    int ho = changeHour();
    int mi = changeMinute();

    if (dateAndTimeChanged == true) {
      // rtc.adjust(DateTime(year, month, date, hour, minute, second));
      rtc.adjust(DateTime(ye, mo, da, ho, mi, 0));
      dateAndTimeChanged = false;
    }
    
  }

  if (digitalRead(P4) == LOW) {
    lcd.clear();
    showAlarmAndTimer();
  }
}

// Shows current date and time
void displayDateTime() {
  DateTime now = rtc.now();
  alarmHourSetter = now.hour();
  alarmMinuteSetter = now.minute();

  getCurrentYear = now.year();
  getCurrentMonth = now.month();
  getCurrentDay = now.day();
  getCurrentHour = now.hour();
  getCurrentMinute = now.minute();

  // Creating buffers and using .toString() for easier formatting
  char buffer_date[] = "DD/MM/YYYY";
  print_lcd_ln("Date: ", 0, 0);
  lcd.println(now.toString(buffer_date));
  
  char buffer_time[] = "hh:mm:ss";
  print_lcd_ln("Time: ", 0, 1);
  lcd.println(now.toString(buffer_time));
  
  lcd.setCursor(14, 1);
  lcd.print(" ");
  lcd.setCursor(15, 1);
  lcd.print(" ");
}

// Set amount of hours for the alarm
int setAlarmHour() {
  // Print info about setting the amount of hours
  print_lcd_ln("Set alarm hour", 0, 0);
  lcd.setCursor(7,1);
  if (alarmHourSetter < 10) {
    lcd.print("0");
    lcd.setCursor(8,1);
    lcd.print(alarmHourSetter);
  } else {
    lcd.println(alarmHourSetter);
    lcd.setCursor(9,1);
    lcd.print("  ");
  }

  // Remove 1 hour from the alarm
  if (digitalRead(P3) == LOW) {
    if (alarmHourSetter < 0) {
      alarmHourSetter = 23;
      delay(200);
    } else {
      alarmHourSetter = alarmHourSetter - 1;
      delay(200);
    }
  }

  // Add 1 hour to the alarm
  if (digitalRead(P4) == LOW) {
    if (alarmHourSetter >= 24) {
      alarmHourSetter = 0;
      delay(200);
    } else {
      alarmHourSetter = alarmHourSetter + 1;
      delay(200);
    }
  }

  // Cancel the alarm
  if (digitalRead(P2) == LOW) {
    lcd.clear();
    print_lcd_ln("Alarm cancelled",0,0);
    alarmActive = false;
    alarmMenu = 0;
    delay(5000);
    return;
  }

  // Set selected hour for the alarm
  if (digitalRead(P1) == LOW) {
    return alarmHourSetter;
    delay(200);
  }
}

// Set amount of minutes for the alarm
int setAlarmMinute() {
  // Needed for checking that we are not setting the alarm at current time;
  // It caused the program to stuck in a long loop few times for some reason
  DateTime now = rtc.now();

  // Print info about setting the amount of minutes
  print_lcd_ln("Set alarm minute", 0, 0);
  lcd.setCursor(7,1);
  if (alarmMinuteSetter < 10) {
    lcd.print("0");
    lcd.setCursor(8,1);
    lcd.print(alarmMinuteSetter);
  } else {
    lcd.println(alarmMinuteSetter);
    lcd.setCursor(9,1);
    lcd.print("  ");
  }

  // Remove 1 minute from the alarm
  if (digitalRead(P3) == LOW) {
    if (alarmMinuteSetter < 0) {
      alarmMinuteSetter = 59;
      delay(200);
    } else {
      alarmMinuteSetter = alarmMinuteSetter - 1;
      delay(200);
    }
  }

  // Add 1 minute to the alarm
  if (digitalRead(P4) == LOW) {
    if (alarmMinuteSetter >= 60) {
      alarmMinuteSetter = 0;
      delay(200);
    } else {
      alarmMinuteSetter = alarmMinuteSetter + 1;
      delay(200);
    }
  }

  // Cancel the alarm
  if (digitalRead(P2) == LOW) {
    lcd.clear();
    print_lcd_ln("Alarm cancelled",0,0);
    alarmActive = false;
    delay(5000);
    alarmMenu = 0;
    return;
  }

  // Set selected minute for the alarm
  if (digitalRead(P1) == LOW && alarmMinuteSetter != now.minute()) {
    alarmActive = true;
    return alarmMinuteSetter;
    delay(200);
  }
}

// Set amount of hours for the timer
// Capping timer at 24 x 7 = 168 hours for a max timer of one week
int setTimerHour() {
  // Print info about setting the amount of hours
  print_lcd_ln("Set timer hour",0,0);
  lcd.setCursor(7,1);
  if (timerHourSetter < 10) {
    lcd.print("0");
    lcd.setCursor(8,1);
    lcd.print(timerHourSetter);
  } else if (timerHourSetter < 100) {
    lcd.println(timerHourSetter);
    lcd.setCursor(9,1);
    lcd.print("  ");
  } else if (timerHourSetter >= 100) {
    lcd.println(timerHourSetter);
    lcd.setCursor(10,1);
    lcd.print("  ");
  }

  // Remove 1 hour from the timer
  if (digitalRead(P3) == LOW) {
    if (timerHourSetter < 0) {
      timerHourSetter = 168;
      delay(200);
    } else {
      timerHourSetter = timerHourSetter - 1;
      delay(200);
    }
  }

  // Add 1 hour to the timer
  if (digitalRead(P4) == LOW) {
    if (timerHourSetter > 168) {
      timerHourSetter = 0;
      delay(200);
    } else {
      timerHourSetter = timerHourSetter + 1;
      delay(200);
    }
  }

  // Cancel the timer
  if (digitalRead(P2) == LOW) {
    lcd.clear();
    print_lcd_ln("Timer cancelled",0,0);
    timerActive = false;
    delay(5000);
    timerMenu = 0;
    return;
  }

  // Set selected hour for the timer
  if (digitalRead(P1) == LOW) {
    timerActive = true;
    return (getCurrentHour + timerHourSetter);
    delay(200);
  }
}

// Set amount of minutes for the timer
int setTimerMinute() {
  // Print info about setting the amount of minutes
  print_lcd_ln("Set timer minute",0,0);
  lcd.setCursor(7,1);
  if (timerMinuteSetter < 10) {
    lcd.print("0");
    lcd.setCursor(8,1);
    lcd.print(timerMinuteSetter);
  } else {
    lcd.println(timerMinuteSetter);
    lcd.setCursor(9,1);
    lcd.print("  ");
  }

  // Remove 1 minute from the timer
  // Setting the minimum time for timer at 1 minute
  if (digitalRead(P3) == LOW) {
    if (timerMinuteSetter < 1) {
      timerMinuteSetter = 59;
      delay(200);
    } else {
      timerMinuteSetter = timerMinuteSetter - 1;
      delay(200);
    }
  }

  // Add 1 minute to the timer
  if (digitalRead(P4) == LOW) {
    if (timerMinuteSetter >= 60) {
      timerMinute = 0;
      delay(200);
    } else {
      timerMinuteSetter = timerMinuteSetter + 1;
      delay(200);
    }
  }
  
  // Cancel the timer
  if (digitalRead(P2) == LOW) {
    lcd.clear();
    print_lcd_ln("Timer cancelled",0,0);
    timerActive = false;
    delay(5000);
    timerMenu = 0;
    return;
  }

  // Set selected hour for the timer
  if (digitalRead(P1) == LOW) {
    timerActive = true;
    return (getCurrentMinute + timerMinuteSetter);
    delay(200);
  }
}

// Activate alarm/timer depending on the type and stop it after a while
void Activate(String type) {
  if (type == "alarm") {
    print_lcd_ln("ALARM ACTIVATED",0,0);
    print_lcd_ln("ALARM ACTIVATED",0,1);
  } else if (type == "timer") {
    print_lcd_ln("TIMER ACTIVATED",0,0);
    print_lcd_ln("TIMER ACTIVATED",0,1);
  }
  // Original values: i=0; i < 20; delay(200);
  for (int i=-60; i < 60; i++) {
    digitalWrite(LLED, HIGH);
    digitalWrite(RLED, HIGH);
    lservo.write(i);
    rservo.write(i);
    tone(buzzer, 1000);
    delay(50);
    digitalWrite(LLED, LOW);
    digitalWrite(RLED, LOW);
    lservo.write(-i);
    rservo.write(-i);
    noTone(buzzer);
    delay(50);
  }
  lcd.clear();
  if (type == "alarm") {
    alarmActive = false;
  } else if (type == "timer") {
    timerActive = false;
  }
}

// Show active alarms and timers
void showAlarmAndTimer() {
  if (alarmActive == true) {
    print_lcd_ln("Alarm:",0,0);
    lcd.setCursor(6,0);
    lcd.print(alarmHour);
    lcd.print(":");
    lcd.print(alarmMinute);
  } else if (alarmActive == false) {
    print_lcd_ln("No alarm",0,0);
  }
  if (timerActive == true) {
    print_lcd_ln("Timer:",0,1);
    lcd.setCursor(6,1);
    lcd.print(timerHour - getCurrentHour);
    lcd.print(":");
    lcd.print(timerMinute - getCurrentMinute);
  }
  delay(10000);
  alarmMenu = 0;
  timerMenu = 0;
}

// Change year 
int changeYear() {
  print_lcd_ln("Set Year",0,0);
  lcd.setCursor(6,1);
  lcd.println(getCurrentYear);

  // Remove 1 year
  if (digitalRead(P3) == LOW) {
    if (getCurrentYear < 0) {
      getCurrentYear = 9999;
      delay(200);
    } else {
      getCurrentYear = getCurrentYear - 1;
      delay(200);
    }
  }

  // Add 1 year
  if (digitalRead(P4) == LOW) {
    if (getCurrentYear > 9999) {
      getCurrentYear = 0;
      delay(200);
    } else {
      getCurrentYear = getCurrentYear + 1;
      delay(200);
    }
  }

  // Cancel date and time setting
  if (digitalRead(P2) == LOW) {
    return;
    delay(200);
  }

  // Set selected year
  if (digitalRead(P1) == LOW) {
    return getCurrentYear;
    delay(200);
  }
}

// Change month
int changeMonth() {
  print_lcd_ln("Set month",0,0);
  lcd.setCursor(7,1);
  if (getCurrentMonth < 10) {
    lcd.print("0");
    lcd.setCursor(8,1);
    lcd.print(getCurrentMonth);
  } else {
    lcd.println(getCurrentMonth);
    lcd.setCursor(9,1);
    lcd.print("  ");
  }

  // Remove 1 month
  if (digitalRead(P3) == LOW) {
    if (getCurrentMonth < 1) {
      getCurrentMonth = 12;
      delay(200);
    } else {
      getCurrentMonth = getCurrentMonth - 1;
    }
  }

  // Add 1 month
  if (digitalRead(P4) == LOW) {
    if (getCurrentMonth > 12) {
      getCurrentMonth = 1;
      delay(200);
    } else {
      getCurrentMonth = getCurrentMonth + 1;
      delay(200);
    }
  }
  
  // Cancel date and time setting
  if (digitalRead(P2) == LOW) {
    return;
    delay(200);
  }

  // Set selected month
  if (digitalRead(P1) == LOW) {
    return getCurrentMonth;
    delay(200);
  }
}

// Change day
int changeDay() {
  print_lcd_ln("Set date",0,0);
  lcd.setCursor(7,1);
  if (getCurrentDay < 10) {
    lcd.print("0");
    lcd.setCursor(8,1);
    lcd.print(getCurrentDay);
  } else {
    lcd.println(getCurrentDay);
    lcd.setCursor(9,1);
    lcd.print("  ");
  }

  // Remove 1 day
  if (digitalRead(P3) == LOW) {
    if (getCurrentDay < 1) {
      if (getCurrentMonth == 1 || 3 || 5 || 7 || 8 || 10 || 12) {
        getCurrentDay = 31;
        delay(200);
      } else if (getCurrentMonth == 4 || 6 || 9 || 11) {
        getCurrentDay = 30;
        delay(200);
      } else if (getCurrentMonth == 2) {
        if (getCurrentYear % 4 == 0) {
          getCurrentDay = 29;
          delay(200);
        } else {
          getCurrentDay = 28;
          delay(200);
        }
      }
    }
  }

  // Add 1 day
  if (digitalRead(P4) == LOW) {
    if (getCurrentMonth == 1 || 3 || 5 || 7 || 8 || 10 || 12) {
      if (getCurrentDay > 31) {
        getCurrentDay = 1;
        delay(200);
      } else {
        getCurrentDay = getCurrentDay + 1;
        delay(200);
      }
    } else if(getCurrentMonth == 4 || 6 || 9 || 11) {
      if (getCurrentDay > 30) {
        getCurrentDay = 1;
        delay(200);
      } else {
        getCurrentDay = getCurrentDay + 1;
        delay(200);
      }
    } else if (getCurrentMonth == 2) {
      if (getCurrentYear % 4 == 0) {
        if (getCurrentDay > 29) {
          getCurrentDay = 1;
          delay(200);
        } else {
          getCurrentDay = getCurrentDay + 1;
          delay(200);
        }
      } else {
        if (getCurrentDay > 28) {
          getCurrentDay = 1;
          delay(200);
        } else {
          getCurrentDay = getCurrentDay + 1;
          delay(200);
        }
      }
    }
  }
  
  // Cancel date and time setting
  if (digitalRead(P2) == LOW) {
    return;
    delay(200);
  }

  // Set selected date
  if (digitalRead(P1) == LOW) {
    return getCurrentDay;
    delay(200);
  }
}

int changeHour() {
  print_lcd_ln("Set hour",0,0);
  lcd.setCursor(7,1);
  if (getCurrentHour < 10) {
    lcd.print("0");
    lcd.setCursor(8,1);
    lcd.print(getCurrentHour);
  } else {
    lcd.println(getCurrentHour);
    lcd.setCursor(9,1);
    lcd.print("  ");
  }

  // Remove 1 hour
  if (digitalRead(P3) == LOW) {
    if (getCurrentHour < 0) {
      getCurrentHour = 23;
      delay(200);
    } else {
      getCurrentHour = getCurrentHour + 1;
    }
  }

  // Add 1 hour
  if (digitalRead(P4) == LOW) {
    if (getCurrentHour >= 24) {
      getCurrentHour = 0;
      delay(200);
    } else {
      getCurrentHour = getCurrentHour + 1;
    }
  }

  // Cancel date and time setting
  if (digitalRead(P2) == LOW) {
    return;
    delay(200);
  }

  // Set selected hour
  if (digitalRead(P1) == LOW) {
    return getCurrentHour;
    delay(200);
  }
}

int changeMinute() {
  print_lcd_ln("Set Minute",0,0);
  lcd.setCursor(7,1);
  if (getCurrentMinute < 10) {
    lcd.print("0");
    lcd.setCursor(8,1);
    lcd.print(getCurrentMinute);
  } else {
    lcd.println(getCurrentMinute);
    lcd.setCursor(9,1);
    lcd.print("  ");
  }

  // Remove 1 minute
  if (digitalRead(P3) == LOW) {
    if (getCurrentMinute < 0) {
      getCurrentMinute = 59;
      delay(200);
    } else {
      getCurrentMinute = getCurrentMinute + 1;
    }
  }

  // Add 1 minute
  if (digitalRead(P4) == LOW) {
    if (getCurrentMinute >= 60) {
      getCurrentMinute = 0;
      delay(200);
    } else {
      getCurrentMinute = getCurrentMinute + 1;
    }
  }

  // Cancel date and time setting
  if (digitalRead(P2) == LOW) {
    return;
    delay(200);
  }

  // Set selected minute
  if (digitalRead(P1) == LOW) {
    dateAndTimeChanged = true;
    return getCurrentMinute;
    delay(200);
  }
}
