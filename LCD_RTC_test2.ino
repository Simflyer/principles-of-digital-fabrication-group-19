// Importing libraries
// Wire allows communication with I2C devices
#include <Wire.h>
// LiquidCrystal_I2C controls LCD
// Recommended Library version for LiquidCrystal_I2C:1.1
// I used version 1.1.2, differences shouldn't be huge... rigth?
#include <LiquidCrystal_I2C.h>

// Library for Real-Time Clock module
#include "RTClib.h"

RTC_DS3231 rtc;

char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

// Set the LCD address to 0x27 for a 16 columns and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);

// LiquidCrystal_I2C library has two print functions:
// lcd.print() prints one character at the cursor location

// lcd.println() prints one line of text but In my use it had a huge flaw: 
// Apparently the function tries to print the whole line into the place of one character,
// which obviously didn't work

// So I made alternative function myself to print one line of text into the LCD
void print_lcd_ln(String lcd_ln) {
  for (int i=0; i < lcd_ln.length(); i = i + 1) {
    lcd.setCursor(i,0);
    lcd.print(lcd_ln.substring(i,i + 1));
  }
}

// When using lcd.println, there will be vertical lines at the end
// Using this function for formatting will remove them
String make_str(char str){
    String new_str = String(str);
    for(int i = 0; i < (16 - new_str.length()); i++) {
        new_str += ' ';  
    }
    return new_str;
}

void setup()
{
  Serial.begin(57600);
  Wire.begin();
  
  // Initialize the LCD
  lcd.init();

  // Turn on the LCD backlight
  lcd.backlight();
  
  // String for testing my function (Must be String type!)
  /*String test_msg = "Hello World!";
  print_lcd_ln(test_msg);
  lcd.setCursor(0,1);
  lcd.println("Line2           ");*/

  
#ifndef ESP8266
  while (!Serial); // wait for serial port to connect. Needed for native USB
#endif

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    Serial.flush();
    while (1) delay(10);
  }

  if (rtc.lostPower()) {
    Serial.println("RTC lost power, let's set the time!");
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }

  // When time needs to be re-set on a previously configured device, the
  // following line sets the RTC to the date & time this sketch was compiled
  // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  // This line sets the RTC with an explicit date & time, for example to set
  // January 21, 2014 at 3am you would call:
  // rtc.adjust(DateTime(2022, 04, 29, 17, 19, 0));
  // LINE ABOVE SHOULD BE RAN ONLY WHEN SETTING TIME  
  
}

void loop()
{
  delay(1000);
  DateTime now = rtc.now();

  // Printing date on the first row of LCD
  lcd.setCursor(0,0);
  lcd.println(now.year(), DEC);
  lcd.setCursor(4,0);
  lcd.print(":");
  lcd.setCursor(5,0);
  if (now.month() >= 10) {
    lcd.println(now.month(), DEC);
  } else {
    lcd.print("0");
    lcd.setCursor(6,0);
    lcd.print(now.month(), DEC);
  }
  lcd.setCursor(7,0);
  lcd.print(":");
  lcd.setCursor(8,0);
  if (now.day() < 10) {
    lcd.print("0");
    lcd.setCursor(9,0);
    lcd.print(now.day(), DEC);
  } else {
    lcd.println(now.day(), DEC);
  }
  for (int i=10; i < 12; i++) {
    lcd.setCursor(i,0);
    lcd.print(" ");
  }

  // Printing time on the second row of LCD
  lcd.setCursor(0,1);
  if(now.hour() < 10) {
    lcd.print("0");
    lcd.setCursor(1,1);
    lcd.print(now.hour(), DEC);
  } else {
    lcd.println(now.hour(), DEC);
  }
  lcd.setCursor(2,1);
  if (now.minute() < 10) {
    lcd.print("0");
    lcd.setCursor(3,1);
    lcd.println(now.minute(), DEC);
  } else {
    lcd.println(now.minute(), DEC);
  }
  lcd.setCursor(5,1),
  lcd.print(":");
  lcd.setCursor(6,1);
  if (now.second() < 10) {
    lcd.print("0");
    lcd.setCursor(7,1);
    lcd.println(now.second(), DEC);
  } else {
    lcd.println(now.second(), DEC);
  }
  
  Serial.print(now.year(), DEC);
  Serial.print('/');
  Serial.print(now.month(), DEC);
  Serial.print('/');
  Serial.print(now.day(), DEC);
  Serial.print(' ');
  Serial.print(now.hour(), DEC);
  Serial.print(':');
  Serial.print(now.minute(), DEC);
  Serial.print(':');
  Serial.print(now.second(), DEC);
  Serial.println();
  
}
