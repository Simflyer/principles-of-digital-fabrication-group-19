#include <Servo.h>          //Servo library
#include <LiquidCrystal.h>  //LCD library
 /* Input:
 * 3 nappia/potikkaa
 * bt ehkä
 * OUTPUT: LED valoille
 * Moottorit, lcd
 */

///////////PINS CONSTANTS AND WHATNOT///////////////////
 
//Servo attributes and constants
int servoInputPin = SET CORRECT PINS HERE;// + to 5V, - to GND, power to DigitalPin N
int servoPos = 0;
Servo servo_handLeft;

//LCD stuff
LiquidCrystal screen(SET CORRECT PINS HERE);

//LED Pins
int ledPin = SET CORRECT PIN HERE;

//Potentiometer pins and value storing
int potentiometerPin = SET CORRECT PINS HERE; // use analogRead(potentiometerPin)
int potentiometerValue = 0;

////////////SETUP///////////////////////////////
void setup() {
  //LEDS
  pinMode(ledPin, OUTPUT);
  //SERVOS
  servo_handLeft.attach(SET CORRECT PIN HERE);
  //LCD
  screen.begin(16 ,2); //Assuming Rows and Columns are 16x2
  //BT?

  //STEPPERS?

  //ACCELOMETERS?

  
 }
///////////MAIN////////////////////
void loop() {
  //Main here
}

///////FUNCTIONS////////////////////////

//lcd screen function, wip
void lcdScreen(input1, input2, input3){
  
  
  screen.setCursor(); //(column, line);

  screen.print("________");
  
}

//function for led blinking, goes through 20 times
void ledBlinkFunction() {
  for(int i = 0; i <= 20; i++){
  digitalWrite(ledPin, HIGH);
  delay(200);
  digitalWrite(ledPin, LOW);
  delay(200);
  }
}

//Function for waving hand, waves 3 times
void servoMotorMoving() {
  for(int i = 0; i <= 3; i++){
     for(servoPos = 0; servoPos <= 90; servoPos = servoPos+1){
       servo_handLeft.write(servoPos);
       delay(100);
     }
     for(servoPos = 90; servoPos >= 0; servoPos = servoPos-1){
       servo_handLeft.write(servoPos);
       delay(100);
     }
  }
}
