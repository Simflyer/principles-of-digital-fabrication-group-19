// Importing libraries
// Wire allows communication with I2C devices
#include <Wire.h>
// LiquidCrystal_I2C controls LCD
// Recommended Library version for LiquidCrystal_I2C:1.1
// I used version 1.1.2, differences shouldn't be huge... rigth?
#include <LiquidCrystal_I2C.h>

// Library for Real-Time Clock module
#include "RTClib.h"

// Library for controlling servo
#include <Servo.h>

RTC_DS3231 rtc;

char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

// Define pins
int P1 = 6; // Set alarm
int P2 = 7; // Set timer
int P3 = 8; // Change time
int P4 = 9; // Show alarm and timer

#define LED 12

#define LSERVO 3
#define RSERVO 5

const int buzzer = 10; // CHECK THE CORRECT PIN!

// define servos
Servo lservo;
Servo rservo;

// Define variables
int getCurrentHour;
int getCurrentMinute;

int alarmHour; // Final hour value of the alarm
int alarmMinute; // Final minute value of the alarm
int alarmHourSetter; // Used in setAlarmHour() to set value for alarmHour
int alarmMinuteSetter; // Used in setAlarmMinute() to set value for alarmMinute
int alarmMenu; // 0=showDateAndClock, 1=setAlarmHour, 2=setAlarmMinute
boolean alarmActive = false;

int timerHour; // Final hour value of the timer
int timerMinute; // Final minute value of the timer
int timerSecond; // Final second value of the timer
int timerHourSetter; // Used in setTimerHour() to set value for timerHour
int timerMinuteSetter; // Used in setTimerMinute() to set value for timerMinute
int timerMenu; // 0=showDateAndClock, 1=setTimerHour, 2=setTimerMinute
boolean timerActive = false;

int servopos = 0;

// Set the LCD address to 0x27 for a 16 columns and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);

// LiquidCrystal_I2C library has two print functions:
// lcd.print() prints one character at the cursor location

// lcd.println() prints one line of text but In my use it had a huge flaw: 
// Apparently the function tries to print the whole line into the place of one character,
// which obviously didn't work

// EDIT: After fixing the connection issues with the LCD and I2C module, the flaw isn't that huge anymore:
// Now it only prints some extra vertical lines after the line, which can be replaced with extra spaces
// I'm still keeping the my own function, since it's handy when writing static text
void print_lcd_ln(String lcd_ln, int column, int row) {
  for (int i=column; i < lcd_ln.length(); i = i + 1) {
    lcd.setCursor(i,row);
    lcd.print(lcd_ln.substring(i,i + 1));
  }
}

void setup()
{
  // Begin Serial
  Serial.begin(57600);

  // Begin I2C modules
  Wire.begin();
  
  // Initialize the LCD
  lcd.init();

  // Turn on the LCD backlight
  lcd.backlight();

  // Clear the LCD
  lcd.clear();

  // Set pinmodes
  pinMode(P1, INPUT_PULLUP);
  pinMode(P2, INPUT_PULLUP);
  pinMode(P3, INPUT_PULLUP);
  pinMode(P4, INPUT_PULLUP);
  pinMode(LED, OUTPUT);
  pinMode(buzzer, OUTPUT);

  // Attach servos to the pins
  lservo.attach(LSERVO);
  rservo.attach(RSERVO);
  
#ifndef ESP8266
  while (!Serial); // wait for serial port to connect. Needed for native USB
#endif

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    Serial.flush();
    while (1) delay(10);
  }

  if (rtc.lostPower()) {
    Serial.println("RTC lost power, let's set the time!");
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }

  // When time needs to be re-set on a previously configured device, the
  // following line sets the RTC to the date & time this sketch was compiled
  // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  // This line sets the RTC with an explicit date & time, for example to set
  // April 29, 2022 at 5:19PM you would call:
  
  // rtc.adjust(DateTime(2022, 04, 29, 17, 19, 0));
  
  // LINE ABOVE SHOULD ONLY BE RAN WHEN SETTING TIME  
}

void loop()
{
  DateTime now = rtc.now();
  
  // Move forward in alarm menu
  if (digitalRead(P1) == LOW) {
    lcd.clear();
    alarmMenu = alarmMenu + 1;
    delay(200);
  }
  if (alarmMenu == 0 && timerMenu == 0) {
    displayDateTime();
  }
  if (alarmMenu == 1) {
    alarmHour = setAlarmHour();
  }
  if (alarmMenu == 2) {
    alarmMinute = setAlarmMinute();
  }
  if (alarmMenu > 2) {
    alarmMenu = 0;
  }

  // Move forward in timer menu
  if (digitalRead(P2) == LOW) {
    lcd.clear();
    timerMenu = timerMenu + 1;
    delay(200);
  }
  
  if (timerMenu == 0 && alarmMenu == 0) {
    displayDateTime();
  }
  if (timerMenu == 1) {
    timerHour = setTimerHour();
  }
  if (timerMenu == 2) {
    timerMinute = setTimerMinute();
  }
  if (timerMenu > 2) {
    timerMenu = 0;
  }

  // Activate active alarm at the correct time and then deactivate it
  if (now.hour() == alarmHour && now.minute() == alarmMinute && alarmActive == true) {
    lcd.clear();
    Activate("alarm");
  }

  // Activate active timer at the correct time and then deactivate it
  if (now.hour() == timerHour && now.minute() == timerMinute && timerActive == true) {
    lcd.clear();
    Activate("timer");
  }

  if (digitalRead(P3) == LOW && alarmMenu == 0 && timerMenu == 0) {
    lcd.clear();
    somethingsomething();
  }

  if (digitalRead(P4) == LOW && alarmMenu == 0 && timerMenu == 0) {
    lcd.clear();
    showAlarmAndTimer();
  }
}

// Shows current date and time
void displayDateTime() {
  DateTime now = rtc.now();
  alarmHourSetter = now.hour();
  alarmMinuteSetter = now.minute();
  
  getCurrentHour = now.hour();
  getCurrentMinute = now.minute();

  // Creating buffers and using .toString() for easier formatting
  char buffer_date[] = "DD/MM/YYYY";
  print_lcd_ln("Date: ", 0, 0);
  lcd.println(now.toString(buffer_date));
  
  char buffer_time[] = "hh:mm:ss";
  print_lcd_ln("Time: ", 0, 1);
  lcd.println(now.toString(buffer_time));
  
  lcd.setCursor(14, 1);
  lcd.print(" ");
  lcd.setCursor(15, 1);
  lcd.print(" ");
}

// Set amount of hours for the alarm
int setAlarmHour() {
  // Print info about setting the amount of hours
  print_lcd_ln("Set alarm hour", 0, 0);
  lcd.setCursor(7,1);
  if (alarmHourSetter < 10) {
    lcd.print("0");
    lcd.setCursor(8,1);
    lcd.print(alarmHourSetter);
  } else {
    lcd.println(alarmHourSetter);
    lcd.setCursor(9,1);
    lcd.print("  ");
  }

  // Remove 1 hour from the alarm
  if (digitalRead(P3) == LOW) {
    if (alarmHourSetter < 0) {
      alarmHourSetter = 23;
      delay(200);
    } else {
      alarmHourSetter = alarmHourSetter - 1;
      delay(200);
    }
  }

  // Add 1 hour to the alarm
  if (digitalRead(P4) == LOW) {
    if (alarmHourSetter >= 24) {
      alarmHourSetter = 0;
      delay(200);
    } else {
      alarmHourSetter = alarmHourSetter + 1;
      delay(200);
    }
  }

  // Cancel the alarm
  if (digitalRead(P2) == LOW && alarmMenu == 1) {
    lcd.clear();
    print_lcd_ln("Alarm cancelled",0,0);
    alarmActive = false;
    alarmMenu = 0;
    delay(2500);
    return;
  }

  // Set selected hour for the alarm
  if (digitalRead(P1) == LOW) {
    delay(200);
    return alarmHourSetter;
  }
}

// Set amount of minutes for the alarm
int setAlarmMinute() {
  // Needed for checking that we are not setting the alarm at current time;
  // It caused the program to stuck in a long loop few times for some reason
  DateTime now = rtc.now();

  // Print info about setting the amount of minutes
  print_lcd_ln("Set alarm minute", 0, 0);
  lcd.setCursor(7,1);
  if (alarmMinuteSetter < 10) {
    lcd.print("0");
    lcd.setCursor(8,1);
    lcd.print(alarmMinuteSetter);
  } else {
    lcd.println(alarmMinuteSetter);
    lcd.setCursor(9,1);
    lcd.print("  ");
  }

  // Remove 1 minute from the alarm
  if (digitalRead(P3) == LOW) {
    if (alarmMinuteSetter < 0) {
      alarmMinuteSetter = 59;
      delay(200);
    } else {
      alarmMinuteSetter = alarmMinuteSetter - 1;
      delay(200);
    }
  }

  // Add 1 minute to the alarm
  if (digitalRead(P4) == LOW) {
    if (alarmMinuteSetter > 59) {
      alarmMinuteSetter = 0;
      delay(200);
    } else {
      alarmMinuteSetter = alarmMinuteSetter + 1;
      delay(200);
    }
  }

  // Cancel the alarm
  if (digitalRead(P2) == LOW && alarmMenu == 2) {
    lcd.clear();
    print_lcd_ln("Alarm cancelled",0,0);
    alarmActive = false;
    delay(2500);
    alarmMenu = 0;
    timerMenu = 0;
    return;
  }

  /// VIRHE ///
  
  // Set selected minute for the alarm
  if (digitalRead(P1) == LOW) {
    alarmActive = true;
    lcd.clear();
    lcd.setCursor(3,0);
    lcd.print("Alarm has");
    lcd.setCursor(3,1);
    lcd.print("been set");
    delay(2500);
    alarmMenu = 0;
    timerMenu = 0;
    return alarmMinuteSetter;
  }
}

// Set amount of hours for the timer
// Capping timer at 24 x 7 = 168 hours for a max timer of one week
int setTimerHour() {
  // Print info about setting the amount of hours
  print_lcd_ln("Set timer hour",0,0);
  lcd.setCursor(7,1);
  if (timerHourSetter < 10) {
    lcd.print("0");
    lcd.setCursor(8,1);
    lcd.print(timerHourSetter);
  } else if (timerHourSetter >= 10) {
    lcd.print(timerHourSetter);
  }

  // Remove 1 hour from the timer
  if (digitalRead(P3) == LOW) {
    if (timerHourSetter <= 0) {
      timerHourSetter = 24;
      delay(200);
    } else {
      timerHourSetter = timerHourSetter - 1;
      delay(200);
    }
  }

  // Add 1 hour to the timer
  if (digitalRead(P4) == LOW) {
    if (timerHourSetter >= 24) {
      timerHourSetter = 0;
      delay(200);
    } else {
      timerHourSetter = timerHourSetter + 1;
      delay(200);
    }
  }

  // Cancel the timer
  if (digitalRead(P1) == LOW && alarmMenu == 0) {
    lcd.clear();
    print_lcd_ln("Timer cancelled",0,0);
    timerActive = false;
    timerHour = 0;
    timerMinute = 0;
    delay(5000);
    timerMenu = 0;
    alarmMenu = 0;
    return;
  }

  // Set selected hour for the timer
  if (digitalRead(P2) == LOW) {
    timerActive = true;
    delay(200);
    return (getCurrentHour + timerHourSetter);
  }
}

// Set amount of minutes for the timer
int setTimerMinute() {
  // Print info about setting the amount of minutes
  print_lcd_ln("Set timer minute",0,0);
  lcd.setCursor(7,1);
  if (timerMinuteSetter < 10) {
    lcd.print("0");
    lcd.setCursor(8,1);
    lcd.print(timerMinuteSetter);
  } else {
    lcd.println(timerMinuteSetter);
    lcd.setCursor(9,1);
    lcd.print("  ");
  }

  // Remove 1 minute from the timer
  // Setting the minimum time for timer at 1 minute
  if (digitalRead(P3) == LOW) {
    if (timerMinuteSetter < 1) {
      timerMinuteSetter = 59;
      delay(200);
    } else {
      timerMinuteSetter = timerMinuteSetter - 1;
      delay(200);
    }
  }

  // Add 1 minute to the timer
  if (digitalRead(P4) == LOW) {
    if (timerMinuteSetter >= 60) {
      timerMinuteSetter = 0;
      delay(200);
    } else {
      timerMinuteSetter = timerMinuteSetter + 1;
      delay(200);
    }
  }
  
  // Cancel the timer
  if (digitalRead(P1) == LOW) {
    lcd.clear();
    print_lcd_ln("Timer cancelled",0,0);
    timerActive = false;
    timerHour = 0;
    timerMinute = 0;
    delay(2500);
    timerMenu = 0;
    alarmMenu = 0;
    return;
  }

  // Set selected minute for the timer
  if (digitalRead(P2) == LOW) {
    timerActive = true;
    lcd.clear();
    lcd.setCursor(3,0);
    lcd.print("Timer has");
    lcd.setCursor(3,1);
    lcd.print("been set");
    delay(2500);
    timerMenu = 0;
    alarmMenu = 0;
    return (getCurrentMinute + timerMinuteSetter);
  }
}

// Activate alarm/timer depending on the type and stop it after a while
void Activate(String type) {
  if (type == "alarm") {
    print_lcd_ln("ALARM ACTIVATED",0,0);
    print_lcd_ln("ALARM ACTIVATED",0,1);
  } else if (type == "timer") {
    print_lcd_ln("TIMER ACTIVATED",0,0);
    print_lcd_ln("TIMER ACTIVATED",0,1);
  }
  digitalWrite(LED, HIGH);
  tone(buzzer, 250);
  for(int i=0; i < 8; i++) {
    for(servopos = 0; servopos <= 30; servopos++) {
      lservo.write(servopos);
      rservo.write(servopos);
      delay(15);
    }
    for (servopos = 30; servopos >= 0; servopos -= 1) {
      lservo.write(servopos);
      rservo.write(servopos);
      delay(15);
    }
  }
  noTone(buzzer);
  digitalWrite(LED, LOW);
  
  lcd.clear();
  if (type == "alarm") {
    alarmActive = false;
  } else if (type == "timer") {
    timerActive = false;
  }
}

// Show active alarms and timers
void showAlarmAndTimer() {
  // Print alarm data
  if (alarmActive == true) {
    print_lcd_ln("Alarm:",0,0);
    lcd.setCursor(6,0);
    if (alarmHour < 10) {
      lcd.print("0");
      lcd.print(alarmHour);
    } else if (alarmHour >= 10) {
      lcd.print(alarmHour);
    }
    lcd.print(":");
    if (alarmMinute < 10) {
      lcd.print("0");
      lcd.print(alarmMinute);
    } else {
      lcd.print(alarmMinute);
    }
  } else if (alarmActive == false) {
    print_lcd_ln("No alarm",0,0);
  }

  // Print timer data
  if (timerActive == true) {
    print_lcd_ln("Timer:",0,1);
    lcd.setCursor(6,1);
    if ((timerHour - getCurrentHour) < 10) {
      lcd.print("0");
      lcd.print(timerHour - getCurrentHour);
    } else if (timerHour >= 10) {
      lcd.print(timerHour - getCurrentHour);
    }
    lcd.print(":");
    if ((timerMinute - getCurrentMinute) < 10) {
      lcd.print("0");
      lcd.print(timerMinute - getCurrentMinute);
    } else {
      lcd.print(timerMinute - getCurrentMinute);
    }
  } else if (timerActive == false) {
    print_lcd_ln("No timer",0,1);
  }
  
  delay(5000);
  alarmMenu = 0;
  timerMenu = 0;
}

void somethingsomething() {
  print_lcd_ln("Yes, this button",0,0);
  print_lcd_ln("works aswell",0,1);
  delay(2500);
  alarmMenu = 0;
  timerMenu = 0;
}
